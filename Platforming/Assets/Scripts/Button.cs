﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Button : MonoBehaviour {
    public string sceneToLoad;

    public void DoClick()
    {
        SceneManager.LoadScene(sceneToLoad); //loads scene when you click button
    }
}
