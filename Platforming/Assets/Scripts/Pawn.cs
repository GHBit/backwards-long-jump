﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Pawn : MonoBehaviour {
    //stats
    public float velocity;
    public float acceleration;
    public float deceleration;
    public float maxSpeed;
    public float jumpForce;
    public float speedToStopAt;
    public float numberOfMidairJumps;
    public float totalNumberOfMidairJumps;
    public bool isGrounded;
    public float distanceFromCenterOfSpriteHorizontalNotExact;
    public float distanceFromCenterOfSpriteVerticalNotExact;

    //animationNames
    public string idleAnimationName;
    public string walkingAnimationName;
    public string jumpingAnimationName;

    //components
    public PlayerAnimator animator;
    public Rigidbody2D rigidBody;
    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        velocity = 0;
        animator = gameObject.GetComponent<PlayerAnimator>();
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        //NOTE 1: size.x * localScale.x is divided by slightly more than 2 to prevent infinite wall jumps, size.y * localScale.y is divided by slightly less than 2 so the raycast spawns just outside the player
        //NOTE 2: size.x or size.y / 2 is the distance from the center to the edge at a scale of 1.
        distanceFromCenterOfSpriteHorizontalNotExact = spriteRenderer.size.x * transform.localScale.x / 2.01f;
        distanceFromCenterOfSpriteVerticalNotExact = spriteRenderer.size.y * transform.localScale.y / 1.99f;
    }

    private void Update()
    {
        //does two short raycasts from slightly below the pawn's sprite to determine if the pawn is grounded
        RaycastHit2D raycastForGroundLeft = Physics2D.Raycast(transform.position + new Vector3(-distanceFromCenterOfSpriteHorizontalNotExact, -distanceFromCenterOfSpriteVerticalNotExact, 0), Vector2.down, 0.1f);
        RaycastHit2D raycastForGroundRight = Physics2D.Raycast(transform.position + new Vector3(distanceFromCenterOfSpriteHorizontalNotExact, -distanceFromCenterOfSpriteVerticalNotExact, 0), Vector2.down, 0.1f);
        if (raycastForGroundLeft.collider != null || raycastForGroundRight.collider != null) //if so set isGrounded to true
        {
            isGrounded = true;
            numberOfMidairJumps = totalNumberOfMidairJumps; //and refills jumps if grounded
        }
        else //otherwise set it to false
        {
            isGrounded = false;
        }
    }

    //default movement
    public void MoveLeftRight(float direction)
    {
        //determines movement speed
        if (direction == 0 && velocity != 0) //if no input is being performed and character is still moving
        {
            velocity -= (velocity / Mathf.Abs(velocity) * deceleration) * Time.deltaTime; //decelerates. velocity / Abs(velocity) determines direction to decelerate.

            //ensures that speed hits 0 after decelerating for long enough
            //this is because without it the player would constantly move slightly back and forth
            if (Mathf.Abs(velocity) < speedToStopAt)
            {
                velocity = 0;
            }
        }
        else
        {
            velocity += direction * acceleration * Time.deltaTime; //otherwise accelerate in the direction you are holding
        }
        //Debug.Log(direction);

        //then caps the speed
        if (velocity < -maxSpeed)
        {
            velocity = -maxSpeed;
        }
        if (velocity > maxSpeed)
        {
            velocity = maxSpeed;
        }

        //then moves
        transform.position += transform.right * velocity * Time.deltaTime;

        //then alters the animation
        if (isGrounded == true)
        {
            if (velocity == 0)
            {
                animator.ChangeAnimation(idleAnimationName); //idle animation if speed is 0
            }
            else if (direction > 0)
            {
                animator.ChangeAnimation(walkingAnimationName, false); //walking animation right if attempting to move right
            }
            else if (direction < 0)
            {
                animator.ChangeAnimation(walkingAnimationName, true); //walking animation left if attempting to move left
            }
        }
    }

    //jumping
    public void Jump()
    {
        if (numberOfMidairJumps > 0 || isGrounded == true) //if the pawn can still jump or is grounded
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpForce); //sets the vertical force of the pawn to the jumping force
        }
        
        if (isGrounded == false) //if not grounded
        {
            numberOfMidairJumps--; //takes away a jump
        }

        animator.ChangeAnimation(jumpingAnimationName); //and sets the animation to the jumping animation
    }
}
