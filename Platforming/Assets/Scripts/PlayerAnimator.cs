﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    Animator animations;
    SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start()
    {
        animations = gameObject.GetComponent<Animator>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    //changes the animation based on a string passed in to this component
    public void ChangeAnimation(string newAnimation)
    {
        animations.Play(newAnimation);
    }

    //changes the animation like above, but also possibly flips the sprite
    public void ChangeAnimation(string newAnimation, bool doFlipSprite)
    {
        ChangeAnimation(newAnimation);
        spriteRenderer.flipX = doFlipSprite;
    }
}
