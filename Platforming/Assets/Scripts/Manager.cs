﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public static Manager me;

    //objects
    public GameObject playerCharacter;
    public AudioSource playerAudioSource;
    public GameObject mainCamera;

    //information
    public float killFloorHeight;
    public Vector2 defaultSpawnPosition;
    public Vector2 currentSpawnPosition;

    //configurations
    public bool manuallySetDefaultSpawnPosition;

    //components of game manager
    public ControlBindings controls;
    
    // Use this for initialization
    void Start()
    {
        controls = GetComponent<ControlBindings>();
    }

    //Next 2 methods ensure that Game Manager continues to work properly after transition between scenes
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLoad;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLoad;
    }

    //Tells the game manager what to grab when loading a scene
    private void OnLoad(Scene scene, LoadSceneMode mode)
    {
        //loads in objects that appear/are removed on a scene loading
        playerCharacter = GameObject.Find("Player");
        if (playerCharacter != null)
        {
            playerAudioSource = playerCharacter.GetComponent<AudioSource>();
        }
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");

        //sets the default spawn position to the player's spawn position, unless otherwise specified
        if (playerCharacter != null && manuallySetDefaultSpawnPosition == false)
        {
            defaultSpawnPosition = playerCharacter.transform.position;
        }
        currentSpawnPosition = defaultSpawnPosition; //sets the current spawn location to default, since the scene was just loaded
    }

    private void Awake()
    {
        if (me == null)
        {
            me = this; //assigns game manager in usable form.
            DontDestroyOnLoad(gameObject); //preserves object between scenes
        }
        else
        {
            Destroy(this.gameObject); //destroys any extra managers
            Debug.Log("Game Manager already exists");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCharacter != null)
        {
            if (mainCamera != null)
            {
                mainCamera.transform.position = playerCharacter.transform.position + new Vector3(0, 0, mainCamera.transform.position.z); //sets the camera's position to the player's position, but zoomed out so that the camera still shows the player and its surrounding objects
            }
            if (playerCharacter.transform.position.y <= killFloorHeight) //if player is below the kill plane
            {
                playerCharacter.transform.position = currentSpawnPosition; //respawns them at the latest checkpoint or the default spawn location
                if (playerAudioSource != null)
                {
                    playerAudioSource.Play(); //and play a sound effect
                }
            }
        }
    }
}
