﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlStuff : Controller
{
    public PlayerPawnStuff pawn;

    private void Start()
    {
        pawn = gameObject.GetComponent<PlayerPawnStuff>();
    }

    private void Update()
    {
        if (pawn != null)
        {
            pawn.MoveLeftRight(Input.GetAxis("Horizontal")); //moves based on horizontal input axis

            if (Input.GetKeyDown(Manager.me.controls.jump)) //if holding up
            {
                pawn.Jump(); //jump
            }
        }
    }
}
