﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {
    public AudioClip soundToPlayOnCheckpoint;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Manager.me.currentSpawnPosition = transform.position;
            AudioSource.PlayClipAtPoint(soundToPlayOnCheckpoint, transform.position);
            Destroy(gameObject);
        }
    }
}
