﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPawnStuff : Pawn
{
    //for floor tiles, sets velocity to 0 when you land on them.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        velocity = 0;
    }

    //for walls and ceilings, removes all momentum while in contact
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (isGrounded == false)
        {
            velocity = 0;
        }
    }
}
